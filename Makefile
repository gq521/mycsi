
target := ttcsi

all : $(target)
.PHONY : clean all

OBJDIR := ./obj
SRCDIR := ./src

CC  := gcc
LD  := gcc
RM  := rm -f -r


CFLAGS := -g -pthread  -Wall -MD -c
LFLAGS := -g -pthread 

srcs :=  $(wildcard $(SRCDIR)/*.c)
objs :=  $(addprefix $(OBJDIR)/, $(notdir $(srcs:.c=.o)))
deps :=  $(objs:.o=.d)

-include $(deps)


$(objs) : $(OBJDIR)/%.o : $(SRCDIR)/%.c
	@[ -d $(OBJDIR) ] || mkdir $(OBJDIR)
	$(CC) -c $(CFLAGS) $< -o $@


$(target) : $(objs)
	$(LD) -o $@  $^  $(LFLAGS)


clean :
	$(RM) $(target)
	$(RM) $(objs)
	$(RM) $(deps)
	
#$(warning $(srcs))
#$(warning $(objs))

