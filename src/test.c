#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <sys/ioctl.h>
#include <fcntl.h>


#define CSI_EN           	(0x00)
#define CSI_CFG         	(0x04)
#define CSI_CAP        		(0x08)
#define CSI_SCALE       	(0x0C)
#define CSI_F0_BUFA     	(0x10)
#define CSI_F0_BUFB     	(0x14)
#define CSI_F1_BUFA     	(0x18)
#define CSI_F1_BUFB     	(0x1C)
#define CSI_F2_BUFA     	(0x20)
#define CSI_F2_BUFB     	(0x24)
#define CSI_BUF_CTRL    	(0x28)
#define CSI_STATUS     		(0x2C)
#define CSI_INT_EN      	(0x30)
#define CSI_INT_STATUS  	(0x34)
#define CSI_HSIZE			(0x40)
#define CSI_VSIZE    		(0x44)
#define CSI_BUF_LEN  		(0x48)



static inline void  write_reg( volatile void *addr, uint32_t val )
{
        asm volatile("str %1, %0"
                     : "+Qo" (*(volatile uint32_t *)addr)
                     : "r" (val));
}


static inline uint32_t read_reg(const volatile void *addr)
{
        uint32_t  val;
        asm volatile("ldr %1, %0"
                     : "+Qo" (*(volatile uint32_t *)addr),
                       "=r" (val));
        return val;
}



/* 返回 NULL 表示错误. */
static inline char * get_uint32( char * pch,  uint32_t * pval )
{
	unsigned long long temp;
	char * ptr;
	
	/* 不能是正负号, 只能是数字, 可以是 0 开头吗 */
	if ( (pch[0] < '0') || (pch[0] > '9') )
	{
		return NULL;
	}

	if ( (pch[0] == '0') && (pch[1] == 'x') )
	{
		pch += 2;
	}
	
	/**/
	temp = strtoull( pch, &ptr, 16 );

	/**/
	if (ptr == NULL)
	{
		return NULL;
	}

	if ( temp >= 0xFFFFFFF0 )
	{
		return NULL;
	}

	*pval = (uint32_t)temp;
	return ptr;
	
}


int  read_phy_addr( char * fname, uint32_t * pval )
{
	int  iret;
	int  fd;
	char  tbuf[100];
	char * pch;

	/**/
	fd = open( fname, O_RDONLY );
	if ( -1 == fd )
	{
		return 1;
	}

	iret = read( fd, tbuf, 40 );
	if ( iret <= 0 )
	{
		return 2;
	}
	close( fd );

	tbuf[iret] = 0;
	pch = get_uint32( tbuf, pval );
	if ( pch == NULL  )
	{
		return 3;
	}

	return 0;
	
}



typedef struct _tag_csi_context
{
	int  tfd;
	
	uint8_t * preg;
	uint8_t * pmem;
	uint32_t  madr;		/**/
	
} csi_contextc_t;



int  csi_init( intptr_t * pret )
{
	int  iret;
	int  tfd;
	csi_contextc_t * pctx;
	void * preg;
	void * pmem;
	uint32_t  madr;

	
	/**/
	tfd = open( "/dev/uio0", O_RDWR );
	if ( tfd < 0 )
	{
		return 1;
	}

    preg = mmap( NULL, 4096, (PROT_READ | PROT_WRITE), MAP_SHARED, tfd, 0 );
    if ( MAP_FAILED == preg )
    {
        return 2;
    }
    
    pmem = mmap( NULL, 0x8000, (PROT_READ | PROT_WRITE), MAP_SHARED, tfd, 0x1000 );
    if ( MAP_FAILED == pmem )
    {
        return 3;
    }

	iret = read_phy_addr( "/sys/devices/virtual/uio/uio0/maps/map1/addr", &madr );
	if ( 0 != iret )
	{
		return 4;
	}
	
	/**/
	pctx = (csi_contextc_t *)malloc( sizeof(csi_contextc_t) );
	if ( NULL == pctx )
	{
		return 5;
	}

	/**/
	pctx->tfd = tfd;
	pctx->preg = (uint8_t *)preg;
	pctx->pmem = (uint8_t *)pmem;
	pctx->madr = madr;


	{
		int  i;

		for ( i=0; i<8192; i++ )
		{
			pctx->pmem[i] = 0;
		}

		
	}
	
	/**/
	*pret = (intptr_t)pctx;
	return 0;
	
}


void  config_format( uint8_t * preg )
{
	uint32_t  temp;

	/* in-fmt=0, out-fmt=0, VREF_POL=1, HREF_POL=1, CLK_POL=1 */
	temp = 0x03;
	write_reg( preg + CSI_CFG, temp );
	return;
}


void  config_size( uint8_t * preg, int hs, int vs )
{
	uint32_t  temp;

	/* HOR_START=0 */
	temp = hs;
	temp = temp << 16;
	temp = temp | 2;
	write_reg( preg + CSI_HSIZE, temp );

	/* VER_START=0 */
	temp = vs;
	temp = temp << 16;
	write_reg( preg + CSI_VSIZE, temp );

	/* BUF_LEN = hor size */
	temp = hs;
	write_reg( preg + CSI_BUF_LEN, temp );
	return;
	
}


void  config_buffer( uint8_t * preg, uint32_t madr )
{
	uint32_t  temp;

	/* buffer A */
	temp = madr + 0;
	write_reg( preg + CSI_F0_BUFA, temp );

	/* buffer B */
	temp = madr + 0x1000;
	write_reg( preg + CSI_F0_BUFB, temp );

	/* buffer A */
	temp = madr + 0;
	write_reg( preg + CSI_F1_BUFA, temp );

	/* buffer B */
	temp = madr + 0x1000;
	write_reg( preg + CSI_F1_BUFB, temp );

	/* buffer A */
	temp = madr + 0;
	write_reg( preg + CSI_F2_BUFA, temp );

	/* buffer B */
	temp = madr + 0x1000;
	write_reg( preg + CSI_F2_BUFB, temp );

	/* buf control */
	temp = 0x01;
	write_reg( preg + CSI_BUF_CTRL, temp );
	return;

}


void  config_inten( uint8_t * preg, int val )
{
	uint32_t  temp;

	/* int enable = 0 */
	if ( val == 0 )
	{
		temp = 0x0;
	}
	else
	{
		temp = 2;
	}
	write_reg( preg + CSI_INT_EN, temp );
	
	return;

}


void  config_capture( uint8_t * preg )
{
	uint32_t  temp;

	/* capture enable = 0 */
	temp = 0x2;
	write_reg( preg + CSI_CAP, temp );

	/**/
	return;

}



int  csi_config( intptr_t ctx )
{
	csi_contextc_t * pctx;
	uint32_t  temp;

	/**/
	pctx = (csi_contextc_t *)ctx;

	/**/
	printf( "addr = %x\n", pctx->madr );

	/**/
	write_reg( pctx->preg + CSI_EN, 0 );
	write_reg( pctx->preg + CSI_EN, 1 );
	/**/
	temp = read_reg( pctx->preg + CSI_EN );
	printf( "enable = %x\n", temp );

	/**/
	config_inten( pctx->preg, 0 );
	config_format( pctx->preg );
	config_size( pctx->preg, 32, 8 );
	config_buffer( pctx->preg, pctx->madr );
	config_capture( pctx->preg );
	config_inten( pctx->preg, 1 );
	
	/**/
	printf( "config complete \n" );
	return 0;
	
}



#if 0

void  tttt_out( uint32_t  data0, uint32_t  data1, uint32_t  data2 )
{
    int  i;
    int  temp;
    
    uint32_t  adc0 = 0;
    uint32_t  adc1 = 0;
    uint32_t  adc2 = 0;
    uint32_t  adc3 = 0;
    
    for ( i=0; i<8; i++ )
    {
        temp = data0 >> 28;
        data0 <<= 4;
        
        /**/
        adc0 <<= 1;
        adc0 |= temp & 1;
        
        temp >>= 1;
        adc1 <<= 1;
        adc1 |= temp & 1;

        temp >>= 1;
        adc2 <<= 1;
        adc2 |= temp & 1;

        temp >>= 1;
        adc3 <<= 1;
        adc3 |= temp & 1;
    }
    
    for ( i=0; i<8; i++ )
    {
        temp = data1 >> 28;
        data1 <<= 4;
        
        /**/
        adc0 <<= 1;
        adc0 |= temp & 1;
        
        temp >>= 1;
        adc1 <<= 1;
        adc1 |= temp & 1;

        temp >>= 1;
        adc2 <<= 1;
        adc2 |= temp & 1;

        temp >>= 1;
        adc3 <<= 1;
        adc3 |= temp & 1;
    }

    for ( i=0; i<8; i++ )
    {
        temp = data2 >> 28;
        data2 <<= 4;
        
        /**/
        adc0 <<= 1;
        adc0 |= temp & 1;
        
        temp >>= 1;
        adc1 <<= 1;
        adc1 |= temp & 1;

        temp >>= 1;
        adc2 <<= 1;
        adc2 |= temp & 1;

        temp >>= 1;
        adc3 <<= 1;
        adc3 |= temp & 1;
    }

    
    printf( "%08x  %08x  %08x  %08x \n", adc0, adc1, adc2, adc3 );
    return;
}

#else

void  tttt_out( uint32_t  data0, uint32_t  data1, uint32_t  data2, uint32_t  data3 )
{
	  printf( "%08x  %08x  %08x  %08x \n", data0>>4, data1>>4, data2>>4, data3>>4 );
}

#endif

int  csi_peek( intptr_t ctx )
{
	int  iret;
	csi_contextc_t * pctx;
	uint32_t  count;
	uint32_t  temp;

	
	/**/
	pctx = (csi_contextc_t *)ctx;


	while ( 1 )
	{
		iret = read( pctx->tfd, &count, sizeof(uint32_t) );
		if ( iret < 0 )
		{
			/**/
			printf( "read ret = %d\n", iret );
			break;
		}

		if ( iret != sizeof(uint32_t) )
		{
			/**/
			printf( "read ret = %d\n", iret );
			break;
		}

		/**/
		temp = read_reg( pctx->preg + CSI_BUF_CTRL );
		if ( 0 == (temp & 0x2) )
		{
				int  i;
				uint32_t * ptr;		
				
				printf( "buffer B \n" );
			
				ptr = (uint32_t *)(pctx->pmem + 4096);
				
				for ( i=0; i<16; i+=4 )
				{
				    tttt_out( ptr[i+0], ptr[i+1], ptr[i+2], ptr[i+3] );
				}
		
		}
		else
		{
				int  i;
				uint32_t * ptr;
	
				printf( "buffer A \n" );
			
				ptr = (uint32_t *)(pctx->pmem);
			
				for ( i=0; i<16; i+=4 )
				{
					tttt_out( ptr[i+0], ptr[i+1], ptr[i+2], ptr[i+3] );
				}
		
		}

	}
	

	return 0;
	
}


int  main( void )
{
	int  iret;
	intptr_t  tsctx;

	/**/
	iret = csi_init( &tsctx );
	if ( 0 != iret )
	{
		printf( "csi_init ret = %d\n", iret );
		return 1;
	}

	iret = csi_config( tsctx );
	if ( 0 != iret )
	{
		printf( "csi_config ret = %d\n", iret );
		return 2;
	}

	/**/
	iret = csi_peek( tsctx );
	if ( 0 != iret )
	{
		printf( "csi_peek ret = %d\n", iret );
		return 3;
	}
	
	return 0;
	
}


